# fig03_01.py

"""Class average program with sequence-controlled repetition."""

# process through each grade to get the sum of all grades
# Then, get the average by dividing the sum by the number of grades

# initialization
grade_counter = 0
total_of_all_grades = 0
grades = [98, 76, 71, 87, 83, 90, 57, 79, 82, 94]

# processing
for grade in grades:
    total_of_all_grades += grade
    grade_counter +=1

# termination
average = total_of_all_grades / grade_counter
print(f'Class average is {average}')
