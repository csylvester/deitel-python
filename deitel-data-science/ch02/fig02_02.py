# fig02_02.py
"""Find the minimum of three values."""

number1 = int(input('Enter first integer: '))
number2 = int(input('Enter second integer: '))
number3 = int(input('Enter third integer: '))

# Assume number1 is the minimum and then compare with the other numbers to update which is min

minimum = number1

if number2 < minimum:
    minimum = number2

if number3 < minimum:
    minimum = number3

print('Minimum value is', minimum)

# Evaluate againusing "min" function

minimum2 = min(number1, number2, number3)
print('Minimum value is', minimum2)

maximum = max(number1, number2, number3)
print('Maximum value is', maximum)

