# Day 2 Project
# Calculate tip amount based on total bill amount
# If more than one diner, divide the total + tip by number of people

# Print a greeting message
# Prompt for total bill amount, e.g, 124.00
# Ask how many diners will be splitting the bill, e.g. 5
# Ask for tip percentage, e.g., 10, 12, or 15
# Calculate how much each diner should pay

print("Welcome to the tip calculator.")

total_bill = float(input("What was the total bill? $"))
number_of_diners = float(input("How many people to split the bill? "))
tip_percentage = float(input("What percentage tip would you like to give? 10, 12, or 15? "))

total_bill_plus_tip = total_bill + (total_bill * (tip_percentage/100.0))

each_diner_amount = total_bill_plus_tip / number_of_diners

print("Each person should pay: $" + str(each_diner_amount))