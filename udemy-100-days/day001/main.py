# Utility to suggest band names
"""Utility to suggest band names"""

# Print a header message, e.g., "Band Name Generator"
# Prompt for name of a pet
# Concatenate the two strings to create a band name

print("Band Name Generator for " + input("What's your name? ") + "\n")

city_name = input("What is the name of a city you've lived in?\n")
pet_name = input("What is the name of one of your pets?\n")

print("\nYour band name could be " + city_name + " " + pet_name + "\n")